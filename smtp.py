#!/usr/bin/python

import simplejson as json
import urllib
import urllib2
import sqlite3
import sys
import datetime
import time
import os
import string
from time import sleep
import re
import glob
from dateutil import parser
import hashlib
from bulbs.neo4jserver import Graph

def mailfrom(data):
  mailfrom = ''
  try:
    mailfrom = [s for s in data if "MAIL FROM:" in s]
    mailfrom = mailfrom[0].split("MAIL FROM:")[1].split('<')[1].split('>')[0]
  except:
    mailfrom = ''
  if (mailfrom == ''):
    try:
      mailfrom = [s for s in data if "MAIL From:" in s]
      mailfrom = mailfrom[0].split("MAIL From:")[1].split('<')[1].split('>')[0]
    except:
      mailfrom = ''
    
  
  return mailfrom

def rcptto(data):
  rcptto = [s for s in data if "RCPT TO:" in s]
  retrcptto = []
  for line in rcptto:
    line=line.split("RCPT TO:")[1].split('<')[1].split('>')[0]
    retrcptto.append(line)
  return retrcptto
  
def subject(data):
  subject=''
  try:
    subject = ([s for s in data if "Subject:" in s])[0].split('Subject:')[1]
  except:
    subject=''
    
  if (subject == ''):
    try:
      subject= ([s for s in data if "subject:" in s])[0].split('subject:')[1]
    except:
      subject=''
      
  subject=subject.strip()
  return subject

def recieved(data):
  recieved=''
  epoch=0
  try:
    recieved=([s for s in lines if "Date:" in s])[0].split('Date:')[1]
  except:
    recieved=''
    
  if (recieved == ''):
    try:
      recieved=([s for s in lines if "Sent:" in s])[0].split('Sent:')[1]
    except:
      recieved=''
  
  if (recieved <> ''):
    try:
      dat = parser.parse(recieved)
      epoch = time.mktime(dat.utctimetuple())
    except:
      epoch=0 #NOT A PERMINANT SOLUTION
  return epoch
  
def vtscan(md5):
  url = "https://www.virustotal.com/vtapi/v2/file/report"
  apikey = "1fcc9f48f2ca0dbdf8182d9a64e19e88e65cf2efffa7571d269d80ecbbbabbdf"
  sleep(15)
  parameters = {"resource": md5,"apikey": apikey}
  data = urllib.urlencode(parameters)
  req = urllib2.Request(url, data)
  response = urllib2.urlopen(req)
  json_response = response.read()
  data = json.loads(json_response)
  if (data['response_code']==1):
      positives=data['positives']
      total=data['total']
      md5=data['md5']
      buildstr = ''
      for result in data['scans']:
        detect=data['scans'][result]['result']
        if (detect<>None):
          buildstr = buildstr + "{} : {},".format(result, detect)
  print("{} has {} detections:\n\n".format(md5,positives))
  return
  
def attach(data):
  fn=''
  attach=''
  ret = []
  try:
    loc = [] #start loc of attachment
    bound = ([s for s in data if "boundary=" in s])[0].strip('''boundary="''') #GET BOUNDARY OF MESSAGE
    attach = ([s for s in data if "Content-Disposition: attachment;" in s])
    if (attach<>''):
      for num in attach:
        loc.append(data.index(num))
        w = data[data.index(num):] #CREATE NEW LIST STARTING FROM ATTACHMENT
        b = ([s for s in w if bound in s])[0] #FIND BOUNDARY
        b = w.index(b) #FIND BOUNDARY END
        w = w[:b] # LIST FROM ATTACH TO BOUNDARY
        fn = ([s for s in w if "filename=" in s])[0]#.strip('''filename=''').strip('''"''')#FILENAME
        if "Content-Disposition: attachment" in fn:
          fn=(fn.split('''filename='''))[1].split(';')[0].strip('''"''')
        if '''";''' in fn:
          fn=(fn.split('''";''')[0])
        if '''"''' in fn:
          fn=(fn.split('''"''')[1])
          
        f=open("attachments/{}".format(fn), 'w+')
        st = (w.index(([s for s in w if "filename=" in s])[0]))+1
        if '''Content-Transfer-Encoding: base64''' in w:
          st = (w.index(([s for s in w if "Content-Transfer-Encoding: base64" in s])[0]))+2
          for line in w[st:]:
            line = line.decode('base64','strict')
            f.write(line)
        else:
			#con = ([s for s in w if "Content-ID" in s])
			#print con
			#if con in w:
			#	print "BLAH"
			for line in w[st:]:
				if ('''Content-ID''' not in line):
				  f.write(line)
				  f.write("\n")
        f.close()
        f=open("attachments/{}".format(fn), 'r') #REOPEN FOR MD5
        dat=f.read()
        md5=hashlib.md5(dat).hexdigest()
        f.close()
        ret.append(fn)
        #vtscan(md5) #UNCOMMENT THIS LINE TO MD5SUM FILES WARNING: THIS WILL BE SLOW
      
  except:
    attach=''
  return ret
    
if __name__=='__main__':
  argslen = len(sys.argv) #Length of args
  g=Graph()
 # index = g.vertices.index.lookup(type="EMAIL") #CHECK IF ROOT NODE EXISTS
 # if (index == None):
 #   index = g.vertices.create(type="EMAIL") #IF NOT CREATE NODE
 # else:
 #   index=list(index)[0] #SET INDEX NODE CREATION
  if (argslen < 2):
      print "PLEASE SPECIFY PCAP"
      exit()
      
  file=sys.argv[1] #SPECIFY FILE
  os.system("tcpflow -r {}".format(file)) #RUN TCPFLOW AGAINST SPECIFIED FILE
  os.system("mkdir attachments")
  toserver=glob.glob("*00025") #FIND SESSIONS TO SERVER END
  
  for file in toserver:
    print ""
    print file #Print Filename
    lines = []
    with open(file) as f:
      try:
        for line in f.readlines():
          lines.append(line.strip())
          f.close()
      except:
        continue
    try:
      f=open(file, 'r') #REOPEN FOR MD5
      fr=f.read()
      md5=hashlib.md5(fr).hexdigest()
      f.close()
      
      fromm = mailfrom(lines) 
      to = rcptto(lines)
      sub = subject(lines)
      dat = recieved(lines)
      att = attach(lines)
      
      id=g.vertices.index.lookup(id=md5) #ADD EMAIL ID
      if (id == None): 
        id = g.vertices.create(id=md5, subject=sub, date=dat)
        #g.edges.create(index, "", id)
      else:
        id=list(id)[0]
        
      neofromm=g.vertices.index.lookup(email=fromm) #ADD FROM
      if (neofromm == None):
        neofromm = g.vertices.create(email=fromm)
        g.edges.create(id, "From", neofromm)
      else:
        neofromm = list(neofromm)[0]
      
      for addr in to: #ADD TO
        print addr
        neoto=g.vertices.index.lookup(email=addr)
        if (neoto == None):
          neoto = g.vertices.create(email=addr)
          g.edges.create(id, "TO", neoto)
        else:
          neoto=list(neoto)[0]
          out=(neoto.inV("TO"))
          if (out <> None):
            if id not in out:
              g.edges.create(id, "TO", neoto)
      
      neosubj=g.vertices.index.lookup(Subject=sub)
      if (neosubj == None):
        neosubj=g.vertices.create(Subject=sub)
        g.edges.create(id, "Subject", neosubj)
      else:
        neosubj=list(neosubj)[0]
        out=list(neosubj.inV("Subject"))
        if (out <> None):
          if id not in out:
            g.edges.create(id, "Subject", neosubj)
      
      for at in att: #ADD TO
        neoatt=g.vertices.index.lookup(File=at)
        if (neoatt == None):
          neoatt = g.vertices.create(File=at)
          g.edges.create(id, "Attachment", neoatt)
        else:
          neoatt=list(neoatt)[0]
          out=list(neoatt.inV("Attachment"))
          if (out <> None):
            if id not in out:
              g.edges.create(id, "Attachment", neoatt)
            
      print "ID: {}".format(md5)
      print "FROM: {}".format(fromm)
      print "TO: {}".format(to)
      print "SUBJECT: {}".format(sub)
      if (dat<>0):
        print "DATE: {}".format(int(dat))
      if (len(att)>0):
        print "Attachments: {}".format(att)
      
      
      
      #email = graph_db.get_or_create_index(neo4j.Node, "Email")
      #mail = graph_db.get_or_create_indexed_node("Email", "ID", {"DATE" : int(dat), "ID" : md5})
      #mail = graph_db.create({"DATE" : int(dat),"ID" : md5})
      #print mail
      #a = graph_db.create({"EMAIL" : fromm})
      #rel = a[0].create_relationship_to(mail[0], "FROM")
      #for line in to:
      #  to = graph_db.create({"EMAIL" : line})
      #  rel = mail[0].create_relationship_to(to[0], "TO")
    except KeyboardInterrupt:
      exit()
